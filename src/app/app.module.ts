import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgZone } from '@angular/core';

//import '@deckikwok/dkgeoloc';
import {DKGoogleMapsAngularModule} from '@deckikwok/dkmaps';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    DKGoogleMapsAngularModule.forRoot(
      'AIzaSyB5cVJxKwUU-Pvh012bgDa_NrFm887dYus'
    ),
    BrowserModule
  ],
  providers: [
    { provide: NgZone, useFactory: () => new NgZone({}) }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
